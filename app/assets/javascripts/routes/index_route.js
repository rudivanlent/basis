Basis.IndexRoute = Ember.Route.extend({
	renderTemplate: function(controller, model){
		this.render('navigation', {outlet: 'navigation'});
		this.render('home', {outlet: 'home'});
	}
});
